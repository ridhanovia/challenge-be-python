import random
import time
import string
import uvicorn
from fastapi import FastAPI, Request
from service import engine
from models import Base
from api import api_router
from starlette.middleware.cors import CORSMiddleware

app = FastAPI(
    title="Challenge",
    docs_url = "/api/docs"
)

app.include_router(api_router, prefix="/api/v1")

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    max_age=3600
)

@app.middleware("http")
async def log_requests(request: Request, call_next):
    idem = "".join(random.choices(string.ascii_uppercase + string.digits, k=6))


    start_time = time.time()
    response = await call_next(request)

    process_time = (time.time() - start_time) * 1000
    formatted_process_time = "{0:.2f}".format(process_time)

    return response

@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        # await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)

@app.on_event("shutdown")
async def shutdown():
    pass

@app.get("/url-list")
def get_all_ursl():
    url_list = [
        {"path": route.path, "name": route.name, "method": list(route.methods)[0]}
        for route in app.routes
    ]
    return url_list

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=5000)