from pydantic import BaseModel

class CreateUser(BaseModel):
    username : str
    name : str
    password : str
    phone_number : str

class VerifPin(BaseModel):
    pin_number : str

class UserLogin(BaseModel):
    username : str
    password : str