from pydantic import BaseModel

class AddTopup(BaseModel):
    account_number : str
    pin_number : str
    transfer_amount : float

class AddTransactions(BaseModel):
    account_number_original : str
    account_number_destination : str
    transfer_amount : float
    pin_number : str

