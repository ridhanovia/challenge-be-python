from models import Users, UserAccount
from sqlalchemy.future import select
from service import ResponseOut, create_access_token
from schema import CreateUser, VerifPin, UserLogin
from datetime import datetime, timedelta

import re
import bcrypt
import random

ACCESS_TOKEN_EXPIRE_MINUTES = 30

#* HASHING PASSWORD
async def hash_password(data):
    password = data.encode('utf-8')
    hash_pass = bcrypt.hashpw(password, bcrypt.gensalt())
    return hash_pass

#* VERIFY PASSWORD
async def verify_password(data, hash_pass):
    password = data.encode('utf-8')
    hash = hash_pass.encode('utf-8')
    check_pass = bcrypt.checkpw(password, hash)
    return check_pass

#* CHECK USERNAME AVAILABILITY
async def check_username(username, db) :
    async with db as session : 
        query_check = await session.execute(select(Users).filter(Users.username == username))
        data = query_check.scalars().first()
        if data is None:
            return True
        else:
            return False
        
#* CREATE USER ACCOUNT
async def create_user_account(username, phone_number, db) :
    async with db as session : 
        try : 

            query_check = await session.execute(select(Users).filter(Users.username == username))
            data = query_check.scalars().first()

            if data is None :
                return ResponseOut("01", f"User {username} not found", [])
            else :
                user_id = data.user_id
                pin_number = "000000"
                balance = 0
                account_number = ''.join([str(random.randint(0, 9)) for _ in range(10)])

                user_account = UserAccount(
                    user_id = user_id,
                    pin_number = pin_number,
                    balance = balance,
                    phone_number = phone_number,
                    account_number = account_number
                )

                session.add(user_account)
                await session.commit()

                return user_account
            
        except Exception as e:

            return ResponseOut("03", f"{str(e)}", [])

        
#* CREATE / REGISTER USER
async def register_user(request : CreateUser, db):
    async with db as session :
        try :
            username = request.username
            name = request.name
            phone_number = request.phone_number
            password = request.password

            hash_pwd = await hash_password(password)
            password_str = hash_pwd.decode('utf-8')

            checked_username = await check_username(username, db)
            if checked_username:
                data = Users(
                    username = username,
                    name = name,
                    password = password_str
                )

                session.add(data)
                await session.commit()

                new_account = await create_user_account(username, phone_number, db)

                if new_account:
                    return ResponseOut("00", "Register Account Success", [{**data.serialize(), 'account_number': new_account.account_number,'balance': new_account.balance}])
                else :
                    return ResponseOut("00", "Register Account Failed", [])
                    
            else :

                return ResponseOut("01", f"User created not successfully, username : {username} has been registered", [])

        except Exception as e:

            return ResponseOut("03", f"{str(e)}", [])
        
#* LOGIN
async def login(request, db):
    async with db as session : 
        try : 
            query_check = select(Users).filter(Users.username == request.username)
            users = await session.execute(query_check)
            data_user = users.scalars().first()

            if data_user and await verify_password(request.password, data_user.password):
                dt = {
                    "user_id": data_user.user_id,
                    "username": data_user.username,
                    "name": data_user.name,
                    "access_date": str(datetime.now())
                }
                _token = create_access_token(data=dt, expires_delta=timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES))

                return ResponseOut("00", "Login Success", data= [{"token_bearer": _token}])
            
            else:

                return ResponseOut("01", f"Login Failed, please check your email or password", [])
            # if data_user and await verify_password(request.password, data_user.password):

            #     return ResponseOut("00", "Login Success", [data_user.serialize()])

        except Exception as e:

            return ResponseOut("03", f"{str(e)}", [])
        

#* VERIFIKASI PIN
async def verifikasi_pin(request : VerifPin, db, _token):
    async with db as session :
        try :
            check_query = select(UserAccount).filter(UserAccount.user_id == _token['user_id'])
            checked_data = await db.execute(check_query)
            data = checked_data.scalars().first()

            if data is None:
                return ResponseOut("02", "Update Failed, Data Not Found", [])
            
            else :
                if data.pin_number != "000000" :
                    return ResponseOut("04", "Pin already verified", [])
                else :
                    if not re.match("^[0-9]{6}$", str(request.pin_number)):
                        return ResponseOut("04", "Invalid pin number format. Must be 6 digit number.", [])
                        
                    
                    data.pin_number = request.pin_number
                    data.updated_at = datetime.now()
                    await session.commit()

                    return ResponseOut("00", "Update Verifikasi Pin Success", [data.serialize()])
            
        except Exception as e:

            return ResponseOut("03", f"{str(e)}", [])

#* GET USER ACCOUNT
async def get_user_account(db, _token):
    async with db as session :
        try : 

            query_user = select(Users).filter(Users.user_id == _token['user_id'])
            user = await session.execute(query_user)
            data_user = user.scalars().first().serialize()

            query_account = select(UserAccount).filter(UserAccount.user_id == _token['user_id'])
            account = await session.execute(query_account)
            data_account = account.scalars().first().serialize()

            data = {**data_user, **data_account}
            return ResponseOut("00", "Get User Account Success", data)

        except Exception as e:

            return ResponseOut("03", f"{str(e)}", [])
        
#* DELETE USER BASED ON USER ID
async def delete_user(user_id, db):
    async with db as session :
        try :

            check_query_user = select(Users).filter(Users.user_id == user_id)
            user = await session.execute(check_query_user)
            data_user = user.scalars().first()
            
            if data_user is None:
                return ResponseOut("02", "Delete Failed, Data Not Found", [])
        
            else :
                check_query_account = select(UserAccount).filter(UserAccount.user_id == user_id)
                account = await db.execute(check_query_account)
                data_account = account.scalars().first()

                data_user.deleted_at = datetime.now()
                data_account.deleted_at = datetime.now()
                await session.commit()

                return ResponseOut("00", "Delete User Account Success", [])
            
        except Exception as e:

            return ResponseOut("03", f"{str(e)}", [])
        

