from models import UserAccount, Transaction
from sqlalchemy.future import select
from sqlalchemy import and_
from service import ResponseOut
from schema import AddTopup, AddTransactions
from datetime import datetime

#* CHECK BALANCE
async def check_balance(account_number, transfer_amount, db):
    async with db as session :
        query_check = await session.execute(select(UserAccount).filter(UserAccount.account_number == account_number))
        data = query_check.scalars().first()

        if data.balance >= transfer_amount :
            return True
        elif data.balance < transfer_amount :
            return False

#* UPDATE BALANCE
async def update_saldo(account_number_original, account_number_destination, transfer_amount, db):
    async with db as session :
        query_rekening_asal = await session.execute(select(UserAccount).filter(UserAccount.account_number == account_number_original))
        data_account_asal = query_rekening_asal.scalars().first()

        query_rekening_destination = await session.execute(select(UserAccount).filter(UserAccount.account_number == account_number_destination))
        data_account_destination = query_rekening_destination.scalars().first()

        data_account_asal.balance -= transfer_amount
        data_account_asal.updated_at = datetime.now()
        data_account_destination.balance += transfer_amount
        data_account_destination.updated_at = datetime.now()
        await session.commit()

        return True
    

# #* CREATE HISTORY TRANSACTION
async def create_history_transaction(user_id, account_number_original, account_number_destination, transfer_amount, type, db):
    async with db as session :
        history = Transaction(
            user_id = user_id,
            transfer_amount = transfer_amount,
            account_number_original = account_number_original,
            account_number_destination = account_number_destination,
            transaction_type = type
        )

        session.add(history)
        await session.commit()

        return history

#* TOPUP
async def topup(request : AddTopup, db, _token):
    async with db as session :
        try :
            user_id = _token['user_id']
            account_number = request.account_number
            pin_number = request.pin_number
            transfer_amount = request.transfer_amount
            limit = 500000000
            type = "topup"

            #* Check if data account is exist
            query_check = await session.execute(select(UserAccount).filter(UserAccount.user_id == user_id))
            data = query_check.scalars().first()

            if data is None : 
                return ResponseOut("02", "User not found", [])
            else :
                if data.account_number != account_number :
                    return ResponseOut("02", "Account number doesn't match ", [])
                else :
                    if data.pin_number == "000000":
                        return ResponseOut("02", "Pin has yet to validated ", [])
                    elif data.pin_number != pin_number :
                        return ResponseOut("02", "Pin doesn't match ", [])
                    else :
                        if transfer_amount > limit :
                            return ResponseOut("02", "Topup limit exceeded ", [])
                        elif transfer_amount < limit :
                            account_number_original = account_number
                            account_number_destination = account_number
                            transfer_amount = transfer_amount

                            data.balance += transfer_amount
                            data.updated_at = datetime.now()
                            await session.commit()

                            data_history = await create_history_transaction(user_id, account_number_original, account_number_destination, transfer_amount, type, db)
                            

                            return ResponseOut("00", "Topup Succeess", [data_history.serialize()])


        except Exception as e:

            return ResponseOut("03", f"{str(e)}", [])
        
#* TRANSFER SALDO
async def transfer(request : AddTransactions, db, _token):
    async with db as session :
        try :
            user_id = _token['user_id']
            account_number_original = request.account_number_original
            account_number_destination = request.account_number_destination
            pin_number = request.pin_number
            transfer_amount = request.transfer_amount
            limit = 500000000
            type = "transfer"

            query_check_user = await session.execute(select(UserAccount).where(
                and_(
                    UserAccount.user_id == user_id,
                    UserAccount.account_number == account_number_original,
                    UserAccount.deleted_at == None
                )))
            data_user = query_check_user.scalars().first()

            query_check_destination = await session.execute(select(UserAccount).where(
                and_(
                    UserAccount.account_number == account_number_destination,
                    UserAccount.deleted_at == None
                )))
            data_destination = query_check_destination.scalars().first()
             
            if data_user is None : 
                return ResponseOut("02", "User not found", [])
            elif data_destination is None :
                return ResponseOut("02", "Account number destination not found", [])
            else :
                if transfer_amount <= limit :
                    check_original_balance = await check_balance(account_number_original, transfer_amount, db)
                    if check_original_balance :
                        if data_user.pin_number == pin_number :
                            data_history = await create_history_transaction(user_id, account_number_original, account_number_destination, transfer_amount, type, db)
                            if data_history is None :
                                return ResponseOut("02", "Cannot make history", [])
                            else :
                                updated_saldo = await update_saldo(account_number_original, account_number_destination, transfer_amount, db)
                                if updated_saldo :
                                    return ResponseOut("00", "Transfer Succeess", [data_history.serialize()])
                                else :
                                    return ResponseOut("02", "Cannot update balance", [])
                        else : 
                            return ResponseOut("02", "Pin doesn't match ", [])
                    else : 
                        return ResponseOut("02", "Insufficient balance ", [])
                    
                elif transfer_amount > limit :
                    return ResponseOut("02", "Topup limit exceeded ", [])
        except Exception as e:

            return ResponseOut("03", f"{str(e)}", [])

#* GET TRANSACTION BASED ON USER ID
async def get_transaction(db, _token):
    async with db as session :
        try :
            query_check = await session.execute(select(Transaction).filter(Transaction.user_id == _token['user_id']))
            histories = query_check.scalars().all()
            data = [history.serialize() for history in histories]
            if data in ([], None):
                return ResponseOut("02", f"List history transaction is Empty", [])
            
            return ResponseOut("00", f"Get all history transactions uccess", data)
        
        except Exception as e:

            return ResponseOut("03", f"{str(e)}", [])
