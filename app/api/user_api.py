from fastapi import APIRouter, Query, Depends
from sqlalchemy.ext.asyncio import AsyncSession 
from schema import CreateUser, VerifPin, UserLogin
from service import get_async_session, verify_token
from crud import register_user, verifikasi_pin, delete_user, get_user_account, login

router = APIRouter()

@router.get("/account", summary = "Get User Account")
async def get_account(db : AsyncSession = Depends(get_async_session), _token : dict = Depends(verify_token)):
    return await get_user_account(db, _token)

@router.post("/register_account")
async def create_user(request : CreateUser, db : AsyncSession = Depends(get_async_session)):
    return await register_user(request, db)

@router.post("/login", )
async def login_user(request : UserLogin, db: AsyncSession = Depends(get_async_session)):
    return await login(request, db)

@router.put("/verifikasi_pin", summary = "Verifikasi Pin ")
async def update_pin(request : VerifPin, db : AsyncSession = Depends(get_async_session), _token : dict = Depends(verify_token)):
    return await verifikasi_pin(request, db, _token)

#* DELETE USER ACCOUNT BASED ON USER ID
@router.put("/account/delete/{user_id}")
async def delete_user_from_admin(user_id : int , db : AsyncSession = Depends(get_async_session), _token : dict = Depends(verify_token)):
    return await delete_user(user_id, db)

