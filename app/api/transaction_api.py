from fastapi import APIRouter, Query, Depends
from sqlalchemy.ext.asyncio import AsyncSession 
from schema import AddTopup, AddTransactions
from service import get_async_session, verify_token
from crud import topup, transfer, get_transaction

router = APIRouter()

@router.get("/history", summary = "Get history transaction")
async def get_history_transactions(db : AsyncSession = Depends(get_async_session), _token : dict = Depends(verify_token)):
    return await get_transaction(db, _token)

@router.post("/topup")
async def new_topup(request : AddTopup, db : AsyncSession = Depends(get_async_session), _token : dict = Depends(verify_token)):
    return await topup(request, db, _token)

@router.post("/transfer")
async def transfer_saldo(request : AddTransactions, db : AsyncSession = Depends(get_async_session), _token : dict = Depends(verify_token)):
    return await transfer(request, db, _token)
