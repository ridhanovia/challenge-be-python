from fastapi import APIRouter

from .user_api import *
from .transaction_api import *

api_router = APIRouter()

api_router.include_router(user_api.router, prefix='/user', tags=['User Account'])
api_router.include_router(transaction_api.router, prefix='/transaction', tags=['Transaction'])