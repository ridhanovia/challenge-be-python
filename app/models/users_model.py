from sqlalchemy import Column, String, BigInteger, Text, DateTime
from datetime import datetime
from service import Base

class Users(Base):
    __tablename__ = 'users'
    user_id = Column(BigInteger, primary_key=True)
    username  = Column(String(50), nullable=False)
    name = Column(String(100), nullable=False)
    password = Column(Text, nullable=False)
    created_at = Column(DateTime, default=datetime.now())
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)
    
    def __init__(self, name, username, password,):
        self.name = name
        self.username = username
        self.password = password

    def serialize(self):
        return {
            'user_id': self.user_id,
            'name': self.name,
            'username': self.username,
        }