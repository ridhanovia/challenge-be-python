from sqlalchemy import Column, String, BigInteger, DateTime, ForeignKey, Float
from datetime import datetime
from service import Base

class UserAccount(Base):
    __tablename__ = 'user_account'
    user_account_id = Column(BigInteger, primary_key=True)
    user_id = Column(BigInteger, ForeignKey('users.user_id'), nullable=False)
    account_number = Column(String(10), nullable=False)
    phone_number = Column(String(15), nullable=False)
    pin_number = Column(String(10), nullable=False)
    balance  = Column(Float, nullable=False)
    created_at = Column(DateTime, default=datetime.now())
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)
    
    def __init__(self, account_number, phone_number, pin_number, balance, user_id):
        self.account_number = account_number
        self.phone_number = phone_number
        self.pin_number = pin_number
        self.balance = balance
        self.user_id = user_id

    def serialize(self):
        return {
            'account_number' : self.account_number,
            'phone_number' : self.phone_number,
            'pin_number' : self.pin_number,
            'balance' : self.balance,
            'user_id': self.user_id,
        }