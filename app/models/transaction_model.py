from sqlalchemy import Column, String, BigInteger, DateTime, ForeignKey, Float
from datetime import datetime
from service import Base

class Transaction(Base):
    __tablename__ = 'transactions'
    transaction_id = Column(BigInteger, primary_key=True)
    user_id = Column(BigInteger, ForeignKey('users.user_id'), nullable=False)
    transfer_amount  = Column(Float, nullable=False)
    account_number_original = Column(String(10), nullable=False)
    account_number_destination = Column(String(10), nullable=False)
    transaction_type = Column(String(10), nullable=False)
    created_at = Column(DateTime, default=datetime.now())
    
    def __init__(self, account_number_original, transfer_amount, user_id, account_number_destination, transaction_type):
        self.account_number_original = account_number_original
        self.account_number_destination = account_number_destination
        self.transaction_type = transaction_type
        self.transfer_amount = transfer_amount
        self.user_id = user_id

    def serialize(self):
        return {
            'account_number_original' : self.account_number_original,
            'account_number_destination' : self.account_number_destination,
            'transaction_type' : self.transaction_type,
            'transfer_amount' : self.transfer_amount,
            'user_id': self.user_id,
            'created_at': self.created_at
        }